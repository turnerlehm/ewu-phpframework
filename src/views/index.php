<html>
<head>
<title>EWU Tools</title>
<link rel="stylesheet" href="homepagestylesheet.css" type="text/css">
</head>
<body>
<form action="index.php" method="post">

<div style="background-color: white; margin: 0; padding: 0;">
<h1 class="centered" style="font-size: 34px; font-family: helvetica;"> <u> Eastern Washington University Tools</u> </h1>
</div>
<hr style="margin-left: 5%; margin-right: 5%;"></hr>

<div style="margin-left: 25%; margin-right: 25%; font-size: 18px;">
	<p>
		The following toolset is provided by the Information Technology department.
	</p>
</div>
<div>
<table class="centered">
<h2 class="centered">Available tools:</h2>
<hr style="margin-left: 5%; margin-right: 5%;"></hr>
<tr></br></tr>

	<!-- SSO -->
	<tr class="tablerow">
		<td><strong><u> Single Sign On</u>: </strong></td>
		<td> Easily integrate EWU SSO into your project with attributes available. </td>
		<td><a href="https://bitbucket.org/easternwashingtonuniversityit/ewu-phpcas">PHP</a></td>
		<td><a href="https://bitbucket.org/easternwashingtonuniversityit/ewu-dotnetcas">.NET</a></td>
		
	</tr>
	
	<!-- Database Management -->
	<tr class="tablerow">
		<td><strong><u> Database Management</u>: </strong></td>
		<td> Manage your database. </td>
		<td><a href="#">DB MANAGEMENT HERE</a></td>
	</tr>
	
	<!-- Database Migrations -->
	<tr class="tablerow">
		<td><strong><u> Database Migration</u>: </strong></td>
		<td> Migrate your database. </td>
		<td><a href="#">DB Migration HERE</a></td>
	</tr>
	
	<!-- Dynamic Form Creation -->
	<tr class="tablerow">
		<td><strong><u> Dynamic Form Creation</u>: </strong></td>
		<td> Dynamically create a form. </td>
		<td><a href="#">Dynamic Form HERE</a></td>
	</tr>
	
	<!-- OAuth -->
	<tr class="tablerow">
		<td><strong><u> OAuth </u>: </strong></td>
		<td> Integration coming soon </td>
		<td><a class="comingSoon" href="#"><strike>Coming Soon</strike></a></td>
	</tr>
</table>
</div>

<hr style="margin-left: 5%; margin-right: 5%;"></hr>

<div class="centered">
	<p>
	For more information, visit our <a href="https://bitbucket.org/easternwashingtonuniversityit/ewu-phpframework" >BitBucket</a> page. 
	</p>
</div>

</body>
</html>
