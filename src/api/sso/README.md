# EWU Single Sign On Library

The EWU SSO library provides a preconfigured implementation of basic single sign-on capabilities for use with PHP based web applications. The library was designed for ease of integration with EWU computer science department senior projects.

The current library supports the following authentication protocols:

  - SAML 1.1
  - CAS 2.0
  - CAS 1.0
  
You may learn more about the supported authentication protocols and single sign-on at the following links:

  - [SAML](https://en.wikipedia.org/wiki/Security_Assertion_Markup_Language)
  - [CAS](https://en.wikipedia.org/wiki/Central_Authentication_Service)
  - [Single Sign-On](https://en.wikipedia.org/wiki/Single_sign-on)


# Installation Requirements
In order to proceed with installing the EWU SSO library you will need:

  - [PHP](http://php.net/manual/en/intro-whatis.php) version [5.3.2 or higher](http://php.net/downloads.php)
  - [Git](https://git-scm.com/)
  - [Composer](https://getcomposer.org/) preferably installed in a 'global' configuration
  
Instructions on how to install these are at the links provided.

**NOTE:** The latest version of PHP supported by the EWU SSO library is version 5.4.16. There are no guarantees that the SSO library will work with versions greater than this. Additionally, this library has only been tested with Linux based web servers and there is no guarantee that the library will work on Windows based servers.

## Installing Composer in a 'global' configuration
For convenvience, instructions on how to install Composer for global use are provided here.

From the directory where you PHP application resides, execute the following command:

```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
```

After that has completed, in the same directory do:

```
sudo php composer-setup.php --install-dir=/bin --filename=composer
```

Composer is now installed globally and can be used by simply using `composer` at the command line.

# Installing the EWU SSO library
Composer makes use of the [Packagist](https://packagist.org/) repository to manage PHP code dependencies. To do so, Composer uses a [JSON](https://en.wikipedia.org/wiki/JSON#Example) formatted file called `composer.json` which lists package dependencies in an easily parsed and human readable way. 

To proceed with installation you will need to create and properly format your own `composer.json` file.

If you have already created and properly formatted a `composer.json` file you can simply do the following **from the root of your project, i.e. where your PHP code and ** `composer.json` ** files are stored/run.**
 
```
composer require ewu/sso:1.0.1
```

If you have not yet created a `composer.json` file in the root of your project, you will need the following in your `composer.json` at a minimum to use the EWU SSO library:

```json
{
	"require": {
		"ewu/sso": "^1.0.1"
	}
}
``` 

For more advanced `composer.json` configurations, see [here](https://knpuniversity.com/screencast/question-answer-day/create-composer-package) and [here](https://scotch.io/tutorials/a-beginners-guide-to-composer#toc-an-introduction-to-composer).

Once you have your `composer.json` file setup and configured with the EWU SSO library listed as a dependency, execute the following command from the root of your project:

```
composer install
```

The EWU SSO library (and any other dependencies listed in your `composer.json` file) will then be downloaded into your project directory and be available for use.

**NOTE: If you did not install Composer in a global configuration you will need to do the following from your project root/where you have your ** `composer.json` ** file and Composer installed:**

```
php composer.phar install
```
 
# Using the EWU SSO library
Once you have installed the EWU SSO library using the steps outlined above, you will need to do the following in order to use it:

1. Add `require './vendor/autoload.php';` to your application's main source file (before any other code or `require` statements).
2. Instantiate/initialize an SSO client, i.e. `$sso = new EWU\SingleSignOn\SSOClient();`
3. Execute the client's `authenticate()` method and retrieve credentials/attributes, i.e. `$results = $sso->authenticate();`

**NOTE: If you installed the EWU SSO library in a directory other than your root project/web directory you will need to change the require in step 1 from ** `require './vendor/autoload.php';` ** to something like ** `require '/path/to/SSO/code/vendor/autoload.php';` **. Relative paths may be used instead.**

## Examples of Use
The following are examples of using the EWU SSO library and the various features provided.

### Basic example

```php
require './vendor/autoload.php';

$sso = new EWU\SingleSignOn\SSOClient();
$results = $sso->authenticate();

# Gets the user's netID
$username = $sso->get_user();
echo "Hello , ".$username.", you are authenticated!";
```

### Retrieving additional attributes using `get_attributes()` and associative arrays

```php
# Get any additional CAS attributes 
$attributes = $sso->get_attributes();
#Alternatively you could do this assuming you retrieved attributes into $results during authentication
#$attributes = $results['attributes'];
ksort($attributes);
foreach ($attributes as $key => $value) {
    if (is_array($value)) {
        echo '<li><strong>', $key, ':<ul>';
        foreach ($value as $item) {
            echo '<li></strong>', $item, '</li>';
        }
        echo '</ul></li>';
    } else {
        echo '<li><strong>', $key, ': </strong>', $value, '</li>'.PHP_EOL;
    }
}
```

This will print out a list of attribute names and their associated values for the authenticated user. From there, you can keep note of attribute names to make accessing them easier using an associative array.

** For example:**

```php
$attributes = $results['attributes'];
$some_user_attribute = $attributes['some_user_attribute_name'];
```

## Within the context of the EWU PHP framework
A basic, preconfigured SSO module built on the EWU SSO libraray is provided with the EWU PHP framework for ease of use and integration.

When creating a new route using this framework that you would like to be protected with SSO, simply add `before($ssoProtect)` to the corresponding Silex `match()`, `get()`, or `post()` statement.

** As an example: **

```php
$app->match('/test', function(){
 return '<strong>This is a test</strong>';
})->before($ssoProtect);
```


By using the Silex `before()` function, we force our application to execute the `ssoProtect()` function before executing the function assoiated with the route we just matched. This in turn forces the user to perform SSO authentication before the function for the matched route is executed.

Due to the structuring of the framework, it may be necessary to `require` the SSO module in your custom code, i.e. adding `require_once '../sso/sso.php';` at the top of your code.
However, if you load your custom code ** AFTER ** the SSO code, you will not have to do this, i.e. in `apiloader.php` the `require_once './your/CustomCode.php';` staement comes after the `require_once './sso/sso.php';` statement.

 
# Protocols
By default, this library uses the SAML 1.1 protocol which enables SSO attributes for authorized services. To change the protocol being used, provide a different protocol into the SSO client's contructor.

 - SAML 1.1 = `'SAML_VERSION_1_1'`
 - CAS 2.0 = `'CAS_VERSION_2_0'`
 - CAS 1.0 = `'CAS_VERSion_1_0'`
 
### Example of different clients using different authentication protocols
```php
require './vendor/autoload.php';

$samlClient = new EWU\SingleSignOn\SSOClient('SAML_VERSION_1_1');
$cas2Client = new EWU\SingleSignOn\SSOClient('CAS_VERSION_2_0');
$cas1Client = new EWU\SingleSignOn\SSOClient('CAS_VERSION_1_0');
```

