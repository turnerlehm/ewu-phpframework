<?php
require_once __DIR__ . '/../../../vendor/autoload.php';

use OAuth\OAuth1\Service\BitBucket;
use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;



$bitbucket_oauth = function(Request $request, Application $app){
  
  require_once __DIR__ . '/bootstrap.php';
  
  
  $storage = new Session();
 
  $credentials = new Credentials(
    $servicesCredentials['bitbucket']['key'],
    $servicesCredentials['bitbucket']['secret'],
    $currentUri->getAbsoluteUri()
  ); 
  
  $bbService = $serviceFactory->createService('BitBucket', $credentials, $storage);
  
  if (!empty($_GET['oauth_token'])) {
  
    $token = $storage->retrieveAccessToken('BitBucket');
    
    // This was a callback request from BitBucket, get the token
    $bbService->requestAccessToken(
        $_GET['oauth_token'],
        $_GET['oauth_verifier'],
        $token->getRequestTokenSecret()
    );
    
    // Send a request now that we have access token
    $result = json_decode($bbService->request('user/repositories'));
    echo $bbService->request('user/repositories');
    //echo('The first repo in the list is ' . $result[0]->name);
    
  } 
  elseif (!empty($_GET['go']) && $_GET['go'] ==  'go') {
  
    // extra request needed for oauth1 to request a request token :-)
    $token = $bbService->requestRequestToken();
    
    $url = $bbService->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));
    
	//print($url);
    //header('Location: ' . $url);
	echo '<script>window.location = "'.$url.'";</script>';
  }
   else {
   
    $url = $currentUri->getRelativeUri() . '?go=go';
    
    echo "<a href='$url'>Login with BitBucket!</a>";
    
  }
  
  return "";
};

$madeIt = function(Request $request, Application $app){
  return 'made it';
};

$app->match('/bit/', $bitbucket_oauth);
$app->match('/bit/madeti', $madeIt);

?>