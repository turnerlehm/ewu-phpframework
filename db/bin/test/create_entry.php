#!/usr/bin/env php
<?php

$app = require_once __DIR__.'/../../../src/bootstrap.php';
$em = $app['orm.em'];

if (count($argv)!=2) {
    echo "usage: create_user <netId>".PHP_EOL;
    return;
}

$netId = $argv[1];
$user = null;


$user=$em->find('Forms\Test\User', $netId) ?
                                           : $user = new Forms\Test\User($netId);

$entry = new Forms\Test\Entry();
$entry->setUser($user);
$entry->setAgree(false);

$em->persist($user);
$em->persist($entry);
$em->flush();

echo var_dump("OK");
