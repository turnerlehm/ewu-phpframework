<?php
// Create the application
$app = new Silex\Application();

// Load the environment
$config_dir = __DIR__; // intent revealing
$config_file = '.env';
$environment = new Dotenv\Dotenv($config_dir); // more intent revealing to use explicit names
if (file_exists($config_dir.'/'.$config_file)) {
    $environment->load();
}
//$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS']);

// Set debug

if (strcasecmp(getenv('DEBUG'), 'true') == 0) {
    $app['debug'] = true;
}

// Register DBAL Service Provider
// If you are using SQLite, you need to set the 'path' variable to the database on the machine
// in the db.options 'sqlite' portion
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array(
		'default' => array(
			'driver' => getenv('FORMS_DB_DRIVER'),
			'host' => getenv('FORMS_DB_HOST'),
			'dbname' => getenv('FORMS_DB_DBNAME'),
			'user' => getenv('FORMS_DB_USER'),
			'password' => getenv('FORMS_DB_PASSWORD')
		),
		'sqlite' => array(
			'driver' => 'pdo_sqlite',
			'path' => __DIR__.'/../db/test.db'
		)
	)
));

// Register ORM Service Provider
$app->register(new Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider(), array(
    'orm.proxies_dir' => __DIR__.'/../db/proxies',
    'orm.em.options' => array(
        'mappings' => array(
            array(
                'type' => 'annotation',
                'namespace' => 'Forms\Test',
                'path' => __DIR__.'/../src/models',
//                'use_simple_annotation_reader' => false,
            ),
        ),
    ),
));

// Register a Twig Service Provider
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// Register a Translation Provider (needed for default form view)
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.messages' => array(),
));

// Register a Validation Service Provider
$app->register(new Silex\Provider\ValidatorServiceProvider());

// Register a Form Service Provider
$app->register(new Silex\Provider\FormServiceProvider());
?>
